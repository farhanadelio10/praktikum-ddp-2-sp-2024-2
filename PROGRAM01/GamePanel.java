
import java.awt.Graphics;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private int xPos = 100, yPos = 100;

	public GamePanel() {
		addKeyListener(new KeyboardInputs(this));
	}

	public void changeXPos(int value) {
		this.xPos += value;
		repaint();
		
	}

	public void changeYPos(int value) {
		this.yPos += value;
		repaint();
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.fillRect(xPos, yPos, 200, 50);
	}
}
