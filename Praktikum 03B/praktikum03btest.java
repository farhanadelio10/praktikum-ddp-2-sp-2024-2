import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;


public class praktikum03btest {
    
    @Test
    public void cariNamatest() {
        String[] daftarNama = {"Amir", "Budi", "Cica", "Dudi", "Erma", "Fifa"};
        String[] daftarNPM = {"0123", "1234", "2345", "3456", "4567", "5678"};
        String inputNPM = "1234";
        String jawabanNama = "Budi";
        assertEquals(jawabanNama, Praktikum03b.cariNama(daftarNama,daftarNPM,inputNPM));


        inputNPM = "9087";
        jawabanNama = "Tidak ada nama dengan NPM " + inputNPM;
        assertEquals(jawabanNama, Praktikum03b.cariNama(daftarNama,daftarNPM,inputNPM));
    }   




}