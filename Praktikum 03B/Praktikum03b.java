public class Praktikum03b {

   public static String cariNama(String[] daftarNama, String[] daftarNPM, String cariNPM ){
    for (int i = 0 ; i < daftarNPM.length ; i++){
        if (daftarNPM[i].equals(cariNPM)){
            return daftarNama[i];
        }
    }
    return "Tidak ada nama dengan NPM "+ cariNPM;
}
    

public static void main(String[] args) {
    Praktikum03b praktik = new Praktikum03b();
    String[] daftarNama = {"Amir", "Budi", "Cica", "Dudi", "Erma", "Fifa"};
    String[] daftarNPM = {"0123", "1234", "2345", "3456", "4567", "5678"};
    System.out.println(praktik.cariNama(daftarNama, daftarNPM, "1234"));

    }
}