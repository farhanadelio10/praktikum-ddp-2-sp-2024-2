public class Player {
    private String name;
    private Hand hand;

    public Player(String name) {
        this.name = name;
        this.hand = new Hand(name);
    }

    public Hand getHand() {
        return hand;
    }

    public boolean play(Eights eights, Hand pile) {
        // Implement player logic here for Crazy 8, returning true if the player wins
        return false; // Placeholder return value
    }

    public boolean playCangkulan(Cangkulan cangkulan, Hand pile) {
        // Implement player logic here for Cangkulan, returning true if the player wins
        // For example, the player can draw a card from cangkulan.getDrawPile() and add to their hand
        // Player can also play a card on the pile and handle the game rules accordingly
        return false; // Placeholder return value
    }

    @Override
    public String toString() {
        return name;
    }
}
