import java.util.ArrayList;
import java.util.Scanner;

public class Eights {

    protected ArrayList<Player> players;
    protected Hand drawPile;
    protected Hand discardPile;
    protected Scanner in;

    public Eights(int numPlayers) {
        Deck deck = new Deck("Deck");
        deck.shuffle();

        // Initialize draw pile and discard pile
        drawPile = new Hand("Draw Pile");
        deck.deal(drawPile, 5 * numPlayers);

        discardPile = new Hand("Discard Pile");
        deck.deal(discardPile, 1);

        // Initialize players
        players = new ArrayList<Player>();
        for (int i = 0; i < numPlayers; i++) {
            players.add(new Player("Player " + (i + 1)));
        }

        // Deal cards to players
        for (Player player : players) {
            deck.deal(player.getHand(), 5);
        }

        in = new Scanner(System.in);
    }

    public void playGame() {
        // Main game loop
        while (true) {
            for (Player player : players) {
                if (player.play(this, discardPile)) {
                    System.out.println(player + " wins!");
                    return;
                }
            }
            if (drawPile.isEmpty()) {
                System.out.println("Draw pile is empty. Game over.");
                return;
            }
        }
    }

    public Hand getDrawPile() {
        return drawPile;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of players (2-7): ");
        int numPlayers = scanner.nextInt();
        while (numPlayers < 2 || numPlayers > 7) {
            System.out.print("Invalid number of players. Enter a number between 2 and 7: ");
            numPlayers = scanner.nextInt();
        }
        Eights game = new Eights(numPlayers);
        game.playGame();
    }
}
