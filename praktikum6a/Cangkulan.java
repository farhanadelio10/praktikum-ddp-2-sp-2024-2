import java.util.Scanner;

public class Cangkulan extends Eights {

    public Cangkulan(int numPlayers) {
        super(numPlayers);
    }

    @Override
    public void playGame() {
        System.out.println("Starting Cangkulan game with " + players.size() + " players.");
        // Implement specific rules for Cangkulan
        while (true) {
            for (Player player : players) {
                if (player.playCangkulan(this, discardPile)) {
                    System.out.println(player + " wins!");
                    return;
                }
            }
            if (drawPile.isEmpty()) {
                System.out.println("Draw pile is empty. Game over.");
                return;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number of players (2-7): ");
        int numPlayers = scanner.nextInt();
        while (numPlayers < 2 || numPlayers > 7) {
            System.out.print("Invalid number of players. Enter a number between 2 and 7: ");
            numPlayers = scanner.nextInt();
        }
        Cangkulan game = new Cangkulan(numPlayers);
        game.playGame();
    }
}
