public class CheckInstance {

    // This method tells us whether the object is an 
    // instance of class whose name is passed as a 
    // string 'c'. 
    public static boolean check(Object obj, String c) 
                      throws ClassNotFoundException 
    { 
        System.out.println("Checking apakah " + obj + "merupakan instance dari " + c);
        return Class.forName(c).isInstance(obj); 
    } 
  
    // Driver code that calls fun() 
    public static void main(String[] args) 
                      throws ClassNotFoundException 
    { 
        Integer i = 5;
  
        // print true as i is instance of class 
        // Integer 
        boolean b = check(i, "java.lang.Integer"); 
  
        // print false as i is not instance of class 
        // String 
        boolean b1 = check(i, "java.lang.String"); 
  
        /* print true as i is also instance of class 
           Number as Integer class extends Number 
           class*/
        boolean b2 = check(i, "java.lang.Number"); 
  
        Manusia amir = new Manusia("Amir");    
        Makhluk airputih = new Air();

        boolean c1 = check(amir, "Manusia");    
        boolean c2 = check(amir, "Bernafas");    
        boolean c3 = check(airputih, "Makhluk");    

        System.out.println(b); 
        System.out.println(b1); 
        System.out.println(b2); 
        System.out.println(c1);
        System.out.println(c2);

        try{
            System.out.println(((Bernafas)amir).nafas());
        }
        catch(ClassCastException e){
            System.out.println(e.getMessage());
            System.out.println(amir + " tidak bisa bernafas");
        }
        

        try{
            System.out.println(((Bernafas)airputih).nafas());
        }
        catch(ClassCastException e){
            System.out.println(e.getMessage());
            System.out.println(airputih + " tidak bisa bernafas");
        }

    } 
} 

abstract class Makhluk {
    
    String nama = "tanpa nama";
    public String getNama(){ return nama;}
}

class Manusia extends Makhluk implements Bersuara, Bernafas{

    Manusia(String nama){
        this.nama = nama;
    }

    @Override
    public String suara(){
        return "Berbicara dengan mulut.";  
    }

    @Override
    public String nafas(){
        return this.toString() + " bernafas dengan hidung dan mulut.";
    }

    @Override
    public String toString(){
        return "Manusia bernama " + nama;
    }
}

class Air extends Makhluk{
    @Override
    public String toString(){
        return "Makhluk ciptaan Tuhan berbentuk air";
    }
}

interface Bersuara{
    public String suara();
}

interface Bernafas{
    public String nafas();
}