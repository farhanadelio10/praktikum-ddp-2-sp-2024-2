public class RightTriangle extends GeometricObject{
    private double alas;
    private double tinggi;

    RightTriangle(){

    }

    public RightTriangle(double alas,double tinggi){
        this.alas = alas;
        this.tinggi = tinggi;

    }

    @Override
    public double getArea(){
        return 0.5 * alas * tinggi ;
    }

    @Override
    public double getPerimeter(){
        return alas + tinggi + Math.sqrt(alas*alas+tinggi*tinggi);
    }

    
}