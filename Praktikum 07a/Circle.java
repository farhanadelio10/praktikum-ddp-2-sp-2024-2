import java.awt.geom.Point2D;

public class Circle extends GeometricObject {
    private double radius;

    public Circle(Point2D.Double position, double radius) {
        super(position);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public Circle clone() {
        Point2D.Double newPosition = new Point2D.Double(getPosition().getX(), getPosition().getY());
        return new Circle(newPosition, this.radius);
    }

    public static void main(String[] args) {
        Point2D.Double position = new Point2D.Double(1.0, 2.0);
        Circle original = new Circle(position, 5.0);

        //deep copy
        Circle deepCopy = original.clone();
        deepCopy.setRadius(15.0);
        System.out.println("Deep copy radius: " + deepCopy.getRadius());
    }
}
