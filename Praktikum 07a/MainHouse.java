class MainHouse {
  public static void main(String[] args) {
    System.out.println("Main Program for House.java!");

    House h1 = new House(1,10);
    House h2 = (House) h1.clone();

    System.out.println("Before changes:");
    System.out.println("h1:"+ h1.toString());
    System.out.println("h2:"+ h2.toString());
  
    h2.setArea(20);
    h2.getWhenBuilt().setYear(1999);

    System.out.println("After changes:");
    System.out.println("h1:"+ h1.toString());
    System.out.println("h2:"+ h2.toString());
     
  }
}