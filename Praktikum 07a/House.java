
public class House implements Cloneable, Comparable<House> {
  private int id;
  private double area;
  private java.util.Date whenBuilt;
  
  public House(int id, double area) {
    this.id = id;
    this.area = area;
    whenBuilt = new java.util.Date();
  }
  
  public int getId() {
    return id;
  }
  public void setArea(double area) {
    this.area= area;
  }

  public double getArea() {
    return area;
  }

  public java.util.Date getWhenBuilt() {
    return whenBuilt;
  }

  //Shallow Copy
  /*
  @Override 
  public Object clone() {
    try {
      return super.clone();
    }
    catch (CloneNotSupportedException ex) {
      return null;
    }
  }*/

  
  //Deep Copy
  @Override 
  public Object clone() {
    House houseClone = new House(id, area);
//    this.whenBuilt = houseClone.getWhenBuilt();
    java.util.Date d = houseClone.getWhenBuilt();
    d.setTime(whenBuilt.getTime());
    return houseClone;
  }
  

  @Override // Implement the compareTo method defined in Comparable
  public int compareTo(House o) {
    if (area > o.area)
      return 1;
    else if (area < o.area)
      return -1;
    else
      return 0;
  }  

  @Override
  public String toString(){
    return "Object House " + id + " with area: " + area + " when built:" + whenBuilt;
  }
}