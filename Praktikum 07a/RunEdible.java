interface CookedFirst{
  public String howToCook();
  
}

public class RunEdible {
  public static void main(String[] args) {
    
    Object[] objects = {new Tiger(), new Chicken(), new Apple(), new Orange()};

    ((Commodity)objects[0]).setPrice(100000);  //tiger
    ((Commodity)objects[1]).setPrice(100);  //chicken
    ((Commodity)objects[2]).setPrice(10);  //apple

    for (int i = 0; i < objects.length; i++) {
      if (objects[i] instanceof Edible)
        System.out.println(((Edible)objects[i]).howToEat());

      if (objects[i] instanceof Animal) 
        System.out.println(((Animal)objects[i]).sound());

      if (objects[i] instanceof Commodity)
        System.out.println(((Commodity)objects[i]).getPrice());
      
    }
  }
}

abstract class Commodity{
  double price = 1.0;
  public void setPrice(double price){
    this.price = price;
  }
  public double getPrice(){
      return price;
  }
}

abstract class Animal extends Commodity {
  private double weight;
  
  public double getWeight() {
    return weight;
  }
  
  public void setWeight(double weight) {
    this.weight = weight;
  }
  
  /** Return animal sound */
  public abstract String sound();

}

class Chicken extends Animal implements Edible,CookedFirst {
  @Override
  public String howToCook(){
    return"Bisa di goreng,di rebus dan lain";
  }

  @Override
  public String howToEat() {
    return "Chicken: Fry it";
  }

  @Override
  public String sound() {
    return "Chicken: cock-a-doodle-doo";
  }
}

class Tiger extends Animal {
  @Override
  public String sound() {
    return "Tiger: RROOAARR";
  }
}

abstract class Fruit extends Commodity implements Edible {
  // Data fields, constructors, and methods omitted here
  
}

class Apple extends Fruit {
  @Override
  public String howToEat() {
    return "Apple: Make apple cider";
  }
}

class Orange extends Fruit{

  @Override
  public String howToEat() {
    return "Orange: Make orange juice";
  }
}