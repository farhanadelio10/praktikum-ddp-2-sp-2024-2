import java.awt.geom.Point2D;

public abstract class GeometricObject implements Cloneable {
    private Point2D.Double position;

    public GeometricObject(Point2D.Double position) {
        this.position = position;
    }

    public Point2D.Double getPosition() {
        return position;
    }

    public void setPosition(Point2D.Double position) {
        this.position = position;
    }

    @Override
    public abstract GeometricObject clone();
}
