
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInputs implements KeyListener {

	private GamePanel gamePanel;

	public KeyboardInputs(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

	public void gamePanel(GamePanel gamePanel){
		this.gamePanel = gamePanel;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.err.println("keyPressed");
		
		int key = e.getKeyCode();
		if(key ==KeyEvent.VK_A ){
			gamePanel.changeXPos(-5);
			
		}

		else if (key == KeyEvent.VK_W){
			gamePanel.changeYPos(-5);
		}

		else if (key == KeyEvent.VK_S){
			gamePanel.changeYPos(5);
		}
		else if (key == KeyEvent.VK_D){
			gamePanel.changeXPos(5);
		}


    }
}
