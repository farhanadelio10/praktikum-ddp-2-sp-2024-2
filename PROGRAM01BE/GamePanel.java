import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private int xPos = 100, yPos = 100;

	public GamePanel() {
		addKeyListener(new KeyboardInputs(this));
	}

	public void changeXPos(int value) {
		this.xPos += value;
		repaint();
		
	}

	public void changeYPos(int value) {
		this.yPos += value;
		repaint();
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(new Color(255, 51,51));
		g.fillOval(xPos, yPos, 200, 50);
	}
}
