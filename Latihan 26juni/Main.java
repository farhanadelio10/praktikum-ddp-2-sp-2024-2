import java.util.Scanner; //scanner is in the java.util package


public class Main {
    public static void main(String[] args){
    int angka;
    int positif = 0;
    int negatif = 0;
    int total = 0;
    int count = 0;

    
    Scanner input = new Scanner(System.in);
    System.out.print("Tuliskan bilangan bulat, dan akhiri dengan 0 : ");
    angka = input.nextInt();
    
    while(angka != 0){
        if (angka > 0){
            positif++;
        }else{
            negatif++;
        }
        total += angka;
        count++;
        angka = input.nextInt();
    }
    input.close();
    double rata = (double) total / count;
    System.out.println("Bilangan positif ada " + positif);
    System.out.println("Bilangan negatif ada " + negatif);
    System.out.println("Total seluruh bilangan adalah " + total);
    System.out.println("Rata-rata seluruh bilangan adalah " + rata);

    }
}