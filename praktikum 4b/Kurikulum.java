public class Kurikulum {
    private ProgramStudi programStudi;
    private int tahun;
    
    public Kurikulum(ProgramStudi programStudi, int tahun) {
        this.programStudi = programStudi;
        this.tahun = tahun;
    }
    
    public ProgramStudi getProgramStudi(){
        return programStudi;
    }
}
