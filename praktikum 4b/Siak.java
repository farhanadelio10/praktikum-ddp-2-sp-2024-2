public class Siak {
    public static void main(String[] args) {


        Kelas a101 = new Kelas("A101");
        Kelas a102 = new Kelas("A102");
        Kelas a103 = new Kelas("A103");
        Kelas a104 = new Kelas("A104");

        ProgramStudi ilKom = new ProgramStudi("Ilmu Komputer");
        ProgramStudi si = new ProgramStudi("Sistem Informasi");

        Kurikulum kurikulumIK2020 = new Kurikulum(ilKom, 2020);
        Kurikulum kurikulumSI2020 = new Kurikulum(si, 2020);

        MataKuliah DDP2 = new MataKuliah("DDP2", 4, a104,kurikulumIK2020);
        MataKuliah SDA = new MataKuliah("SDA", 4, a103,kurikulumIK2020);
        MataKuliah PPL = new MataKuliah("PPL", 4, a102,kurikulumIK2020);
        MataKuliah APAP = new MataKuliah("APAP", 4, a101,kurikulumSI2020);

        MataKuliah[] jadwalSenin = {DDP2,SDA};
        MataKuliah[] jadwalSelasa = {PPL,APAP};

        Jadwal senin = new Jadwal("Senin", jadwalSenin);
        Jadwal selasa = new Jadwal("Selasa", jadwalSelasa);

        Jadwal[] jadwal = {senin,selasa};
        Mahasiswa mahasiswa = new Mahasiswa("2306240162", "Farhan Adelio");
        mahasiswa.setGPA(3.90);
        mahasiswa.setIsRegular(true);
        mahasiswa.setJadwal(jadwal);
    }
    
}
