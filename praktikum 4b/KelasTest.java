import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class KelasTest {

    @Test 
    public void getLantaiKelas(){
        Kelas kelas = new Kelas("A203");

        assertEquals('2', kelas.getLantai());
    }

}
