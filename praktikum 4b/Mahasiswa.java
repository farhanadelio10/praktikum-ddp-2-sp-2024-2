public class Mahasiswa {
    private String npm;
    private String nama;
    private double GPA;
    private Boolean isRegular;
    private Jadwal[] jadwal;
    private ProgramStudi programStudi;

    public Mahasiswa(String studentId, String nama){
        this.npm = studentId;
        this.nama = nama;
    }

    public int bayarSPP(){
        return 10000000;
    }

    public void setJadwal(Jadwal[] jadwal){
        this.jadwal = jadwal;
    }

    public void setGPA(double gPA) {
        GPA = gPA;
    }

    public void setIsRegular(Boolean isRegular) {
        this.isRegular = isRegular;
    }    

    public void setProstud(ProgramStudi programStudi){
        this.programStudi = programStudi;
    }
}

