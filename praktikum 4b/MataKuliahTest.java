import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MataKuliahTest {
    @Test
    public void getNamaKelas(){
        Kelas kelas = new Kelas("a104");
        ProgramStudi kurikulumIK2020 = new ProgramStudi("Farsilkom");
        Kurikulum kurikulum = new Kurikulum(kurikulumIK2020, 2020);
        MataKuliah DDP2 = new MataKuliah("DDP2", 4, kelas, kurikulum);
        assertEquals("DDP2", DDP2.getNamaMatkul());
        
    }
}
