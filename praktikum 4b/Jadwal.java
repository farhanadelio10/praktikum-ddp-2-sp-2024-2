public class Jadwal {
    private String hari;
    private MataKuliah[] jadwalMataKuliah;
    
    public Jadwal(String hari, MataKuliah[] jadwalMataKuliah) {
        this.hari = hari;
        this.jadwalMataKuliah = jadwalMataKuliah;
    }

    public MataKuliah[] getJadwal(){
        return jadwalMataKuliah;
    }

    
}
