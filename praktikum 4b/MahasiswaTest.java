import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MahasiswaTest {
    @Test
    public void bayarSPPSukses(){
        Mahasiswa mahasiswa = new Mahasiswa("2306240162", "Farhan Adelio");
        mahasiswa.setIsRegular(true);
        int bayar = mahasiswa.bayarSPP();
        assertEquals(10000000,bayar);
    }

}
