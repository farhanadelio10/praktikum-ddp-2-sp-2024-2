import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class KurikulumTest {
    @Test
    public void String(){
        ProgramStudi FKG = new ProgramStudi("FKG");
        Kurikulum kurikulumIK2020 = new Kurikulum(FKG, 2020);
        assertEquals("FKG", kurikulumIK2020.getProgramStudi().getNama());

    }
}
