public class MataKuliah {
    private String namaMatkul;
    private int sks;
    private Kelas kelas;
    private Kurikulum kurikulum;

    public MataKuliah(String namaMatkul, int sks, Kelas kelas,Kurikulum kurikulum ){
        this.namaMatkul = namaMatkul;
        this.sks = sks;
        this.kelas = kelas;
    }

    public void setKurikulum(Kurikulum kurikulum){
        this.kurikulum = kurikulum;
    }
    public String getNamaMatkul(){
        return namaMatkul;
    }

}
