import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorGeometryTest {
@Test
public void luasLingkaranTest(){
    assertEquals(314.159,CalculatorGeometry.hitungLuasLingkaran(10),0.001);
    }

@Test
public void kelilingLingkaranTest(){
    assertEquals(62.8318,CalculatorGeometry.hitungKelilingLingkaran(10),0.001);
}

@Test
public void luasPersegiTest(){
    assertEquals(35,CalculatorGeometry.hitungLuasPersegi(5,7));
    }
@Test
public void kelilingPersegiTest(){
    assertEquals(30,CalculatorGeometry.hitungKelilingPersegi(10,5));
}

@Test
public void LuasSegitiga(){
    assertEquals(50,CalculatorGeometry.hitungLuasPersegi(10, 5) );
}
@Test
public void kelilingSegitiga(){
    assertEquals(15, CalculatorGeometry.hitungKelilingSegitiga(5));
}
@Test
public void luasJajarGenjang(){
    assertEquals(35,CalculatorGeometry.hitungLuasJajarGenjang(5,7));
}

@Test
public void luasTrapesium(){
    assertEquals(35, CalculatorGeometry.hitungLuasTrapesium(5,6,7),0.00001);
}

}