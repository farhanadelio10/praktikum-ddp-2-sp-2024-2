import static org.junit.Assert.*;
import org.junit.Test;

/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2306240162
    No Ruang / Kursi: 104 // 11
*/

public class Soal1Test {
    @Test
    public void testBukanSegitiga00(){
        Soal1 segitiga = new Soal1(1,2,4);
        assertEquals("Bukan Segitiga", segitiga.jenisSegitiga());
    }

    @Test
    public void testBukanSegitiga01(){
        Soal1 segitiga = new Soal1(4,2,1);
        assertEquals("Bukan Segitiga", segitiga.jenisSegitiga());
    }

    @Test
    public void testBukanSegitiga02(){
        Soal1 segitiga = new Soal1(2,4,1);
        assertEquals("Bukan Segitiga", segitiga.jenisSegitiga());
    }

    @Test
    public void testSamaSisi00(){
        Soal1 segitiga = new Soal1(4,4,4);
        assertEquals("Sama sisi", segitiga.jenisSegitiga());
    }

    @Test
    public void testSamaKaki00(){
        Soal1 segitiga = new Soal1(4,2,4);
        assertEquals("Sama kaki", segitiga.jenisSegitiga());
    }

    @Test
    public void testSamaKaki01(){
        Soal1 segitiga = new Soal1(2,4,4);
        assertEquals("Sama kaki", segitiga.jenisSegitiga());
    }

    @Test
    public void testSamaKaki02(){
        Soal1 segitiga = new Soal1(4,4,2);
        assertEquals("Sama kaki", segitiga.jenisSegitiga());
    }

    @Test
    public void testSembarang00(){
        Soal1 segitiga = new Soal1(4,3,7);
        assertEquals("Sembarang", segitiga.jenisSegitiga());
    }

    @Test
    public void testSembarang01(){
        Soal1 segitiga = new Soal1(7,3,4);
        assertEquals("Sembarang", segitiga.jenisSegitiga());
    }

    @Test
    public void testSembarang02(){
        Soal1 segitiga = new Soal1(4,7,3);
        assertEquals("Sembarang", segitiga.jenisSegitiga());
    }



}
