import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2306240162
    No Ruang / Kursi: 104 / 11
*/

// import static org.junit.jupiter.api.Assertions.assertEquals;
// import org.junit.jupiter.api.Test;

public class BahanBakarTest {

    @Test
    public void testHargaBahanBakar() {
        
        assertEquals(61000, Soal4.hargaBahanBakar(5, 12300));
        
        assertEquals(198400, Soal4.hargaBahanBakar(8, 25000));
        
        assertEquals(279500, Soal4.hargaBahanBakar(5, 56000));
        
    }
}
