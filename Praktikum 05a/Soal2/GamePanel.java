
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private float xDelta = 100, yDelta = 100;
	private float xDir = 1f, yDir = 1f;
	private int frames = 0;
	private long lastCheck = 0;

	private Color color = new Color(150,50,90);

	private Random random;
	public GamePanel() {

		random = new Random();
		addKeyListener(new KeyboardInputs(this));
	}
	

	public void changeXPos(int value) {
		this.xDelta += value;
		
	}

	public void changeYPos(int value) {
		this.yDelta += value;
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		updateRectangle();
		g.setColor(color);
		g.fillRect((int)xDelta, (int)yDelta, 200, 50);
		g.fillOval((int) xDelta + 100, (int) yDelta + 100, 100, 50);
		



	}
	public void updateRectangle() {
		xDelta += xDir;
		if (xDelta > 400 || xDelta < 0){
			xDir *= -1;
			color = setRndColor();
		}
		yDelta += yDir;
		if (yDelta > 400 || yDelta < 0){
			yDir *= -1;
			color = setRndColor();
		}

	}
	public Color setRndColor() {
		int r = random.nextInt(255);
		int g = random.nextInt(255);
		int b = random.nextInt(255);

		return new Color(r, g, b);
	}

}
