
/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2306240162
    No Ruang / Kursi: 104 / 11
*/


public class Soal1 {
    private int a;
    private int b;
    private int c;

    Soal1(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String jenisSegitiga(){

        if (a==b && a==c && b==c){
            return ("Sama sisi");
        }
        else if(a==b || a==c || b==c){
            return("Sama kaki");
        }
        else if (a+b < c || a+c < b || b+c < a ) {
            return("Bukan Segitiga");
        }
        else return("Sembarang");
    }

    public static void main(String[] argv){

        for (int i=0;i<10;i++){

            // Menggenerate bilangan mulai dari 1-30
            int a = (int)(Math.random()*30)+1;
            int b = (int)(Math.random()*30)+1;
            int c = (int)(Math.random()*30)+1;

            System.out.printf("Sisi %d, %d, %d adalah ",a,b,c);

            Soal1 segitiga = new Soal1(a,b,c);
            System.out.println(segitiga.jenisSegitiga());    
        }
    }

}
