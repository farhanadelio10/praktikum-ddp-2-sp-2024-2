import static org.junit.Assert.*;

import java.beans.Transient;

import org.junit.Test;

/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2306240152
    No Ruang / Kursi: 104 / 11
*/

public class Soal3Test {
    // @Test
    // public void test_00(){
    //     // Bisakah anda membuat test case?
    //     // Anda bisa menggunakan assertEquals() untuk membandingkan 
    //     // nilai yang diharapkan dan 
    //     // nilai yang didapat dari method yang di-test
    //     // Contoh: assertEquals(4, 2+2);  // apakah penjumlahan 2+2 = 4
    //     assertEquals(4, 2+2);
    // }

    // @Test
    // public void test_01(){
    //     int[] expected = {1,2,3};
    //     int[] actuals  = {3,4,5};
    //     assertArrayEquals(expected, actuals);
    // }

    @Test
    public void testGambarMatrix1(){
        int [][] actuals = {
                        {1,2,3,4},
                        {4,5,6,7},
                        {8,9,1,2},
                        {3,4,5,6}
        };

        int[][] expected= {
                        {1,2,3,0},
                        {4,5,0,7},
                        {8,0,1,2},
                        {0,4,5,6}
        };
        assertArrayEquals(expected,Soal3.GambarMatrix(actuals));
    }

    @Test
    public void testGambarMatrix2(){
        int [][] actuals = {
                        {7,7,7,7,7},
                        {7,7,7,7,7},
                        {7,7,7,7,7},
                        {7,7,7,7,7},
                        {7,7,7,7,7}
        };

        int[][] expected= {
                        {7,7,7,7,0},
                        {7,7,7,0,7},
                        {7,7,0,7,7},
                        {7,0,7,7,7},
                        {0,7,7,7,7}
        };
        assertArrayEquals(expected,Soal3.GambarMatrix(actuals));

    }
}
