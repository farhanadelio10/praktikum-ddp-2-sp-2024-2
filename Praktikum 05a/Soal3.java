
/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2305240162
    No Ruang / Kursi: 104 / 11
*/


public class Soal3 {
    public static int[][] GambarMatrix(int[][] arrayMatrix){
        int baris = arrayMatrix.length;
        int kolom = arrayMatrix[0].length;

        for (int i = 0; i < baris ; i++){
            for(int j = 0 ; j < kolom ; j++ ){
                if (i + j == baris -1){
                    arrayMatrix[i][j] = 0;
                }
            }
        }
        return arrayMatrix;
    }
}

