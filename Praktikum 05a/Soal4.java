
/*
    Baca soal pada Dokumen Soal-UTS-DDP2-17Juli2024.md (.pdf)

    Tuliskan Nama dan NPM anda di bawah ini:
    Nama: Farhan Adelio Prayata
    NPM : 2306240162
    No Ruang / Kursi: 104 / 11
*/


public class Soal4 {

    public static int hargaBahanBakar(int litres, int pricePerLitre) {
        // Hitung total harga awal
        int totalHargaAwal = litres * pricePerLitre;
        
        // Hitung diskon per liter
        int diskonPerLiter = 0;
        if (litres >= 2) {
            diskonPerLiter = (litres / 2) * 50;
            if (diskonPerLiter > 250) {
                diskonPerLiter = 250;
            }
        }
        
        // Hitung total diskon
        int totalDiskon = litres * diskonPerLiter;
        
        // Hitung total harga akhir
        int totalHargaAkhir = totalHargaAwal - totalDiskon;
        
        return totalHargaAkhir;
    }
}
