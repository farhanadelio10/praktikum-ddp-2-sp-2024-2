public class DDWallet extends DDAccount {

    public DDWallet(String accountNumber, String accountName, double balance) {
        super(accountNumber, accountName, balance);
    }
    
    public boolean pay(int amount) {
        return withdraw(amount);
    }

    @Override
    public void addInterest() {
        double balance = getBalance();
        if (balance < 50_000_000) {
            setBalance(balance + balance * 0.07 / 100);
        } else if (balance < 100_000_000) {
            setBalance(balance + balance * 0.08 / 100);
        } else {
            setBalance(balance + balance * 0.20 / 100);
        }
    }
}
