import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DDAccountTest {
    private DDAccount account;

    @BeforeEach
    public void setUp() {
        account = new DDAccount("123456789", "John Doe", 1_000_000.0);
    }

    @Test
    public void testWithdrawSufficientBalance() {
        assertTrue(account.withdraw(500_000.0));
        assertEquals(500_000.0, account.getBalance());
    }

    @Test
    public void testWithdrawInsufficientBalance() {
        assertFalse(account.withdraw(1_500_000.0));
        assertEquals(1_000_000.0, account.getBalance());
    }
}
