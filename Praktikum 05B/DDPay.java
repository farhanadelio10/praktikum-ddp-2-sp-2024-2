import java.util.ArrayList;

public class DDPay {
    int month = 1;
    ArrayList<DDAccount> accounts = new ArrayList<>();
    
    public static void main(String[] args) {
        DDPay d = new DDPay();
        d.accounts.add(new DDWallet("DDAW-1", "Budi", 1_000_000));
        d.accounts.add(new DDWallet("DDAW-2", "Santi", 100_000));
        d.accounts.add(new DDWallet("DDAW-3", "Andi", 5_000_000));
        d.accounts.add(new DDWallet("DDAW-4", "Ilsa", 500_000));

        System.out.println(d.nextMonth());
        d.pay(d.accounts.get(0), 50_000);

        System.out.println(d.nextMonth());
        d.withdraw(d.accounts.get(2), 1_000_000);
        
        System.out.println(d.nextMonth());
        d.pay(d.accounts.get(1), 50_000);

        System.out.println(d.nextMonth());
        d.withdraw(d.accounts.get(2), 1_000_000);
        d.pay(d.accounts.get(0), 50_000);

        System.out.println(d.nextMonth());
    }

    public String nextMonth() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Summary pada bulan ke-").append(month).append("\n");
        for (DDAccount customer : accounts) {
            sb.append(customer);
            sb.append("\n");
        }
        sb.append("====================================\n");

        // Add interest to all accounts
        for (DDAccount customer : accounts) {
            customer.addInterest();
        }
        
        month++;
        return sb.toString();
    }

    public void pay(DDAccount account, int amount) {
        if (account instanceof DDWallet) {
            DDWallet wallet = (DDWallet) account;
            if (wallet.pay(amount)) {
                System.out.println(account.getAccountNumber() + " - Rp" + amount + " - Pembayaran berhasil\n");
            } else {
                System.out.println(account.getAccountNumber() + " - Rp" + amount + " - Pembayaran gagal\n");
            }
        }
    }
    
    public void withdraw(DDAccount account, int amount) {
        if (account.withdraw(amount)) {
            System.out.println(account.getAccountNumber() + " - Rp" + amount + " - Penarikan berhasil\n");
        } else {
            System.out.println(account.getAccountNumber() + " - Rp" + amount + " - Penarikan gagal\n");
        }
    }
}
