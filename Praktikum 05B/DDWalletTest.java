import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DDWalletTest {
    private DDWallet walletAccount;

    @BeforeEach
    public void setUp() {
        walletAccount = new DDWallet("987654321", "Alice", 30_000_000.0);
    }

    @Test
    public void testPayWithSufficientBalance() {
        boolean result = walletAccount.pay(5_000_000);
        assertTrue(result, "Payment should be successful with sufficient balance.");
        assertEquals(25_000_000.0, walletAccount.getBalance());
    }

    @Test
    public void testPayWithInsufficientBalance() {
        boolean result = walletAccount.pay(35_000_000);
        assertFalse(result, "Payment should fail with insufficient balance.");
        assertEquals(30_000_000.0, walletAccount.getBalance());
    }

    @Test
    public void testAddInterestWithBalanceAfterThreeMonths() {
        for (int i = 0; i < 3; i++) {
            walletAccount.addInterest();
        }
        double expectedBalance = 30_000_000.0;
        expectedBalance += 21_000 + 21_014.70 + 21_028.41;
        assertEquals(expectedBalance, walletAccount.getBalance(), 0.01);
    }
}
