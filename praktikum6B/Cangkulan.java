
import java.util.Scanner;

public class Cangkulan extends Eights{
    public Cangkulan(String[] playerNames){
        super(playerNames);

    }

    @Override
    public boolean cardMatches(Card card1, Card card2) {
        return card1.getSuit() == card2.getSuit()
            || card1.getRank() == card2.getRank();
        
    }
        


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] predefinedNames = {"abbtt", "pudan", "kieval", "aisyah", "tampubolon", "shelley", "abby"};

        System.out.print("Enter number of players (2-7): ");
        int numPlayers = scanner.nextInt();

        if (numPlayers < 2 || numPlayers > 7) {
            System.out.println("Number of players must be between 2 and 7.");
            
        }
        
        String[] playerNames = new String[numPlayers];
        System.arraycopy(predefinedNames, 0, playerNames, 0, numPlayers);
        
        Cangkulan game = new Cangkulan(playerNames);
        game.playGame();
        scanner.close();
    }

}