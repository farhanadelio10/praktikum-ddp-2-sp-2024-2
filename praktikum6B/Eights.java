
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Eights {

    private List<Player> players; //list untuk menyimpan players
    private Hand drawPile;
    protected Hand discardPile;
    private Scanner in;

    public Eights(String[] playerNames) {
        Deck deck = new Deck("Deck");
        deck.shuffle();

        // mendapatkan nama playernya
        players = new ArrayList<>();
        for (int i = 0; i < playerNames.length; i++) {
            Player player = new Player(playerNames[i]);
            deck.deal(player.getHand(), 5);
            players.add(player);
        }

        // turn one card face up
        discardPile = new Hand("Discards");
        deck.deal(discardPile, 1);

        // put the rest of the deck face down
        drawPile = new Hand("Draw pile");
        deck.dealAll(drawPile);

        // create the scanner 
        // we'll use to wait for the user to press enter
        in = new Scanner(System.in);
    }

    public boolean cardMatches(Card card1, Card card2) {
        return card1.getSuit() == card2.getSuit()
            || card1.getRank() == card2.getRank()
            || card1.getRank() == 8;
    }

    /**
     * Returns true if either hand is empty. (udah)
     */
    public boolean isDone() {
        for (Player player : players){
            if (player.getHand().isEmpty()){
                return true;
            }
        }
        return false;
    }

    /**
     * Moves cards from the discard pile to the draw pile and shuffles.
     */
    public void reshuffle() {
        // save the top card
        Card prev = discardPile.popCard();

        // move the rest of the cards
        discardPile.dealAll(drawPile);

        // put the top card back
        discardPile.addCard(prev);

        // shuffle the draw pile
        drawPile.shuffle();
    }

    /**
     * Returns a card from the draw pile.
     */
    public Card drawCard() {
        if (drawPile.isEmpty()) {
            reshuffle();
        }
        return drawPile.popCard();
    }

    /**
     * Switches players.
     */
    public Player nextPlayer(Player current) {
        int index = players.indexOf(current);
        return players.get((index + 1) % players.size());
    }

    /**
     * Displays the state of the game.
     */
    public void displayState() {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).display();
        }

        discardPile.display();
        System.out.println("Draw pile: " + drawPile.size() + " cards");
        in.nextLine();
    }

    /**
     * One player takes a turn.
     */
    public void takeTurn(Player player) {
        Card prev = discardPile.lastCard();
        Card next = player.play(this, prev);
        discardPile.addCard(next);

        System.out.println(player.getName() + " plays " + next);
        System.out.println();
    }

    /**
     * Plays the game.
     */
    
    public void playGame() {
        Player player = players.get(0);

        // keep playing until there's a winner
        while (!isDone()) {
            displayState();
            takeTurn(player);
            player = nextPlayer(player);
        }

        // display the final score
        for (int i = 0; i < players.size(); i++) {
            players.get(i).displayScore();
        }
        saveScoreToFile();
    }

    private void saveScoreToFile() {
        Player winner = null;
        for (Player player : players){
            if (player.getHand().isEmpty()){
                winner = player;
                break;
            }
        } 
        // 6b
        if (winner != null) {
            try (FileWriter writer = new FileWriter("highscores-eight" + ".txt")) {
                for (Player player : players) {
                    writer.write(player.getName() + ": " + player.score() + "\n");
                }
            } catch (IOException e) {
                System.err.println("An error occurred while saving scores to file.");
                e.printStackTrace();
            }
        }

        // 6a
        if (winner != null) {
            try (FileWriter writer = new FileWriter(winner.getName() + ".txt")) {
                // for (Player player : players) {
                //     writer.write(player.getName() + ": " + player.score() + "\n");
                // }
                writer.write(winner.getName() + ": " + winner.score());
            } catch (IOException e) {
                System.err.println("An error occurred while saving scores to file.");
                e.printStackTrace();
            }
        }
        terbesarTerurut();

    }

    //6c
    public void terbesarTerurut(){
        List<Player> sorted = players.stream()
                            .sorted(Comparator.comparingInt(Player::getScore))
                            .collect(Collectors.toList());
        for (int i = 0; i < players.size(); i++) {
            System.out.println(sorted.get(i).getName() + " : " + sorted.get(i).getScore());
        }
    }

    /**
     * Creates the game and runs it.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] predefinedNames = {"axel", "pudan", "kieval", "aisyah", "tampubolon", "shelley", "abby"};

        System.out.print("Enter number of players (2-7): ");
        int numPlayers = scanner.nextInt();

        if (numPlayers < 2 || numPlayers > 7) {
            System.out.println("Number of players must be between 2 and 7.");
            return;
        }

        String[] playerNames = new String[numPlayers];
        System.arraycopy(predefinedNames, 0, playerNames, 0, numPlayers);

        Eights game = new Eights(playerNames);
        game.playGame();
        scanner.close();
    }

}
