/**
 * Test code for Deck and Hand.
 */
public class RunDrawPile {

    public static void main(String[] args) {
        Deck deck = new Deck("Deck"); // 
        deck.shuffle();

        Hand hand = new Hand("Hand"); // object 
        deck.deal(hand, 3); //membagikan kartu sebanyak 5 ke objek hand
        hand.display();

        Hand hand2 = new Hand("Hand"); // object 
        deck.deal(hand2, 2); //membagikan kartu sebanyak 2 ke objek hand
        hand2.display();

        Hand hand3 = new Hand("Hand"); // object 
        deck.deal(hand3, 4); //membagikan kartu sebanyak 4 ke objek hand
        hand3.display();

        Hand hand4 = new Hand("Hand"); // object 
        deck.deal(hand4, 3); //membagikan kartu sebanyak 3 ke objek hand
        hand4.display();

        Hand drawPile = new Hand("Draw Pile");
        deck.dealAll(drawPile);
        System.out.printf("Draw Pile has %d cards.\n", drawPile.size());
    }

}
