package Lat01b;

public class Tugas2 {

    public static boolean prima(int angka) {
        if (angka < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(angka); i++) {
            if (angka % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void tampilkanPrima(int batas) {
        int count = 0; 
        int primaCount = 0; 

        System.out.println("Bilangan prima dari 2 sampai " + batas + ":");

        for (int angka = 2; angka <= batas; angka++) {
            if (prima(angka)) {
                System.out.print(angka + " ");
                count++;
                primaCount++;
                if (count == 8) {
                    System.out.println();
                    count = 0;
                }
            }
        }

        if (count != 0) {
            System.out.println();
        }

        System.out.println("Jumlah bilangan prima dari 2 sampai " + batas + " adalah: " + primaCount);
    }

        public static void main(String[] args) {
        int batas = 250; 
        tampilkanPrima(batas);
    }
}
