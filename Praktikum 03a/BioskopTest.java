import static org.junit.Assert.*;
import org.junit.Test;

public class BioskopTest {

    @Test 
    public void pemesananTest01(){
        // TODO
        String[][] bioskopLama = {
            {"X","0","0"},
            {"0","X","X"}
        };
        String pesan = "A2";

        String[][] bioskopBaru = {
            {"X","0","0"},
            {"0","X","X"}
        };

        assertArrayEquals(bioskopBaru,Bioskop.pesanKursi(pesan, bioskopLama));
    }

    @Test 
    public void hitungKursi01() {
        String[][] bioskop = {
            {"X", "0", "X"},
            {"X", "X", "0"}
        };
        int jmlhKursiDipesan = 3; // Expected number of booked seats

        assertEquals(jmlhKursiDipesan, Bioskop.hitungTerisi(bioskop));
    }   

    @Test 
    public void hitungPendapatan01() {
        String[][] bioskop = {
            {"X", "0", "X"},
            {"X", "X", "0"}
        };
        double expectedPendapatan = 3 * 5000000;

        assertEquals(expectedPendapatan, Bioskop.hitungPendapatan(bioskop), 0.0);
}

    }

    // TODO Tambahkan test-test lainnya. Untuk satu fungsi perlu beberapa test

