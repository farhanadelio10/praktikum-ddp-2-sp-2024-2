import java.util.Scanner;

/**
 * Kelas ini digunakan untuk memproses fitur-fitur yang ada pada Sistem
 * Pemesanan Kursi Bioskop Om Burhan
 * Fitur yang ada pada kelas ini adalah:
 * 1. Pesan Kursi
 * 2. Tampilkan Peta Kursi 
 * 0. Selesai : akan menampikan peta, jumlah dipesan dan pendapatan
 * 
 * ANDA BISA MEMODIFIKASI PARAMATER YANG ADA PADA METHOD YANG SUDAH DISEDIAKAN
 * ANDA BEBAS MEMODIFIKASI BARIS KODE MANA SAJA, TIDAK HANYA PADA BAGIAN TODO
 * 
 * Perhatikan indeks pada peta bioskop mulai dari 1, sementara index mulai dari 0
 * 
 * Anda bisa memanfaatkan fungsi:
 *  split: "12x8".split("x") = {"12","8"}
 *  charAt: "A1".charAt(0)-'A' = 0
 *  charAt: "B2".charAt(1)-'1' = =
 * 
 */
public class Bioskop {
    public static void main(String[] args) {
        String[][] bioskop; //TODO lengkapi inisilasasi-nya
        
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan ukuran bioskop anda (baris x kolom): ");
        String inisialisasiBioskop = input.nextLine();

        // Jika inputan sesuai dengan format, maka program akan berjalan
        if (inisialisasiBioskop.matches("[0-9]x[0-9]")) {
            String myStr = inisialisasiBioskop;
            String regex = "x";
            String[] myArray = myStr.split(regex);
            int Baris = Integer.valueOf(myArray[0]);
            int Kolom = Integer.valueOf(myArray[1]);
            bioskop = new String[Baris][Kolom];
            // TODO: Lengkapi kode di bawah ini
            for (int i= 0; i < bioskop.length; i++){
                for (int j = 0; j < bioskop[i].length; j++){
                    bioskop[i][j] = "0";
                }
            
            }

            tampilkanPetaKursi(bioskop); // Mencetak peta kursi awal

            System.out.println("\nSelamat datang di Sistem Pemesanan Kursi Bioskop Om Burhan!");
            System.out.println("Menu ");
            System.out.println("1. Pesan Kursi\n2. Tampilkan Peta Kursi\n0. Selesai");
            String perintah = "";

            do {
                System.out.print("Pilih menu: ");
                perintah = input.nextLine();
                if (perintah.equals("1")) {
                    System.out.print("Masukan nomor kursi: ");
                    String kursi = " ";
                    while (!kursi.equals("X")){
                        kursi = input.next();
                        if (!kursi.equals("X") ){
                        pesanKursi(kursi, bioskop);}
                    }
                    input.nextLine();
                } else if (perintah.equals("2")) {
                    tampilkanPetaKursi(bioskop);
                } else {
                    if (!(perintah.equals("0"))) {
                        System.out.println("Input tidak valid. Silahkan coba lagi!");
                    }
                }
                System.out.println();
            } while (!(perintah.equals("0")));

            informasiSelesai(bioskop);

        } else {
            System.out.println("Input tidak valid!");
        }
        input.close();
    }

    /**
     * Method ini digunakan untuk memproses fitur Pesan Kursi
     * 
     * @param kursi   Kursi yang akan dipesan
     * @param bioskop Peta pemesanan kursi di bioskop
     * @return  Peta pemesanan kursi baru setelah pemesanan
     */
    public static String[][] pesanKursi(String kursi, String[][] bioskop) {
        // TODO: Implementasi fitur Pesan Kursi
        //Kursi "A2" --> baris = 0, kolom = 1
        int baris = kursi.charAt(0)-'A';
        int kolom = kursi.charAt(1)-'1';
        bioskop[baris] [kolom] = "X";
        return bioskop;    

    }

    /**
     * Method ini digunakan untuk memproses fitur Tampilkan Peta Kursi
     * 
     * @param bioskop Peta pemesanan kursi di bioskop
     */
    public static void tampilkanPetaKursi(String[][] bioskop) {
        String[] arrayAlpha = {"A","B","C", "D", "E", "F", "G","H","I","J","K","L"};
        for (int i= 0; i < bioskop.length; i++){  
            System.out.print(arrayAlpha[i] + " ");
            for (int j = 0; j < bioskop[i].length; j++){
                System.out.print(bioskop[i][j] + " ");
            }
            System.out.println();
  
        }
        System.out.print("  ");
        for (int i= 0; i < bioskop[0].length; i++){ 
            System.out.print((i+1) + " ");
        }
    }

    /**
     * Method ini digunakan untuk memproses fitur Selesai
     * 
     * @param bioskop Peta pemesanan kursi di bioskop
     */
    public static void informasiSelesai(String[][] bioskop) {
        System.out.println("Terima kasih telah menggunakan Sistem Pemesanan Kursi Bioskop Om Burhan!");
        System.out.println("Total kursi terisi: " + hitungTerisi(bioskop));
        // TODO: Hitung jumlah kursi yang terisi

        System.out.print("Pendapatan: ");
        System.out.printf("Rp.%,d,00\n", (int)hitungPendapatan(bioskop));
        // TODO: Hitung total pendapatan (1 Kursi = Rp50.000)
        // TODO: Ketika mencetak gunakan formating yang tepat
        //       sehingga tertulis Rp. 50.000,00
        //       Silahkan check: https://docs.oracle.com/javase/tutorial/i18n/format/numberintro.html
        //       sebagai basis yang kuat, sebelum sumber lain yang lebih sederhana
        System.out.println(hitungPendapatan(bioskop));
        tampilkanPetaKursi(bioskop);
    }

    /**
     * Method ini untuk menghitung total kursi yang terisi
     * 
     * @param bioskop Peta pemesanan kursi di bioskop
     * @return jumlah kursi yang terisi
     */
    public static int hitungTerisi(String[][] bioskop){
        int terisi = 0;
        for (int i = 0; i < bioskop.length; i++){
            for (int j = 0; j < bioskop[i].length; j++){
                if (bioskop[i][j].equals("X")){
                    terisi ++;
                }
            }
        }
            
        return terisi;
    }

 
    /**
     * Method ini untuk pendapatan dari pemesanan dengan satu kursi
     * seharga Rp. 50.000,-
     * 
     * @param bioskop Peta pemesanan kursi di bioskop
     * @return jumlah total rupiah yang diterima 
     */
    public static double hitungPendapatan(String[][] bioskop){
        // double pendapatan = 0.0 ;
        // int terisi = hitungTerisi(bioskop);
        return hitungTerisi(bioskop) * 50000.0;


    }

}