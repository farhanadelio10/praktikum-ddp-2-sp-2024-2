import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private int xPos = 100, yPos = 100;
	private Color color = new Color(150,20,30);
	private Random random;

	public GamePanel() {
		random = new Random();
		addKeyListener(new KeyboardInputs(this));
	}

	public void changeXPos(int value) {
		this.xPos += value;
		repaint();
		
	}

	public void changeYPos(int value) {
		this.yPos += value;
		repaint();
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		updateColor();
		g.setColor(color);
		setBackground(color.BLACK);
		g.fillOval(xPos, yPos, 200, 50);

	}

	private void updateColor(){
		if (xPos == xPos){
			color = getRandomColor();
			}
	}

	private Color getRandomColor(){
		int r =random.nextInt(255);
		int g =random.nextInt(255);;
		int b =random.nextInt(255);

		return new Color(r,g,b);
	}
}
