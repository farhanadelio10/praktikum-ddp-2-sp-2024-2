public class MaxUsingGenericType {
  /** Return the maximum between two objects */
  public static <E extends Comparable<E>> E max(E o1, E o2) {
    if (o1.compareTo(o2) > 0)
      return o1;
    else
      return o2;
  }

    public static void main(String[] args) throws ClassNotFoundException{
      int x = 23;
      String text = "Welcome";
//      System.out.println(max(text,x)); //compile error, bukan runtime error seperti di Max.java!

      Human h1 = new Human("Ade");
      Human h2 = new Human("Budi");
      System.out.println(max(text,"Ahlan"));
      System.out.println(max(43,x));
      System.out.println(max(h1,h2));
      System.out.println(Class.forName("Human").isInstance(h1));
      System.out.println(Class.forName("Human").isInstance(text));
    }
}

class Human implements Comparable<Human>{
  String nama;
  Human(String nama){
    this.nama = nama;
  }

  Human(){
    nama = "Budi";
  }
  public String getNama(){
    return nama;
  }

  public int compareTo(Human x){
    return nama.compareTo(x.getNama());
  }

  public String toString(){
    return "Human bernama "+ nama;
  }

}