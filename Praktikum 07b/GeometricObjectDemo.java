import java.util.*;

public class GeometricObjectDemo{
  public static void main(String[] args ) {
    Rectangle rectangle = new Rectangle(2, 2);
    Circle circle = new Circle (2);
    SikuSiku siku = new SikuSiku(2,4);
    System.out.println("Same area? " + equalArea(rectangle, circle));
    System.out.println("Same area? " + equalArea(rectangle, siku));

    ArrayList<GeometricObject> kumpulan = new ArrayList<GeometricObject>();

    kumpulan.add(rectangle);
    kumpulan.add(circle);
    kumpulan.add(siku);
    System.out.println("Same area? " + equalArea(kumpulan.get(0), kumpulan.get(2)));

  }

  public static <E extends GeometricObject> boolean    
        equalArea(E object1, E object2) {
      return object1.getArea() == object2.getArea();
  }
}

class SikuSiku extends GeometricObject{
  private double tinggi;
  private double alas;
  SikuSiku(){
    tinggi=2;
    alas=2;
  }
  SikuSiku(double tinggi, double alas){
    this.tinggi=tinggi;
    this.alas=alas;
  }
  
  public double getArea(){
    return tinggi*alas/2;
  }

  public double getPerimeter(){
    return tinggi+alas+Math.sqrt(tinggi*tinggi+alas*alas);
  }
}