public class SuperWildCardDemo {
  public static void main(String[] args) {
    GenericStack<String> stack1 = new GenericStack<String>();
    GenericStack<Object> stack2 = new GenericStack<Object>();
    stack2.push("Java");
    stack2.push(2);
    stack1.push("Sun");
    add(stack1, stack2);
    AnyWildCardDemo.print(stack2);
  }

  // Perhatikan <? super T> Lihat contoh dibawah.
  public static <T> void add(GenericStack<T> stack1, GenericStack<? super T> stack2) {
    while (!stack1.isEmpty())
      stack2.push(stack1.pop());
  }

  /*
   * List<? extends Number> foo1 = new ArrayList<Number>(); // Number "extends" Number (in this context) 
   * List<? extends Number> foo2 = new ArrayList<Integer>(); // Integer extends Number 
   * List<? extends Number> foo3 = new ArrayList<Double>(); // Double extends Number
   * 
   *
   * List<? super Integer> fuu1 = new ArrayList<Integer>(); // Integer is a "superclass" of Integer (in this context) 
   * List<? super Integer> fuu2 = new ArrayList<Number>(); // Number is a superclass of Integer 
  * List<? super Integer> fuu3 = new ArrayList<Object>(); // Object is a superclass of Integer
   * 
   */

}
