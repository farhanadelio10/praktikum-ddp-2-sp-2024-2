public class FruitDemo {
  public static void main(String[] argv) {
    Fruit fruit1 = new Apple();
    Fruit fruit2 = new Orange();

    Apple apple;
    apple = (Apple) fruit1;
    // apple = (Apple) fruit2; // Compile ok, runtime error!
    if (fruit2 instanceof Apple)
      apple = (Apple) fruit2; // Compile ok, runtime ok!

    System.out.println("Program berjalan baik.");
  }
}

class Apple extends Fruit {

}

class Orange extends Fruit {

}

abstract class Fruit {

}